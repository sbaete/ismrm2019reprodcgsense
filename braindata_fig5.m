%% ISMRM 2019 reproducible research study group
%  2019 reproduce a seminal paper initiative
% https://blog.ismrm.org/2019/04/02/ismrm-reproducible-research-study-group-2019-reproduce-a-seminal-paper-initiative/

% By: Steven Baete (steven.baete@nyulangone.org)
%     NYU LMC CBI/CAI2R 
%     April 2019

% Reproduce figure 5 of [1] with the brain data supplied. 
%
% [1] Pruessmann, K. P.; Weiger, M.; Boernert, P. and Boesiger, P.
% Advances in sensitivity encoding with arbitrary k-space trajectories.
% Magn Reson Med 46: 638-651 (2001)

clear all; clc; close all; 

restoredefaultpath
% Path to toolbox Jeff Fessler for NUFFT
path(path,genpath('irt/'));

% local scripts
path(path,genpath('gpuNUFFT-master/'));
path(path,'panel/');

%% Load data
rawdata_real    = h5read('rawdata_brain_radial_96proj_12ch.h5','/rawdata');
trajectory      = h5read('rawdata_brain_radial_96proj_12ch.h5','/trajectory');

rawdata = rawdata_real.r+1i*rawdata_real.i; clear rawdata_real;
k = trajectory(:,:,1) + 1j* trajectory(:,:,2);
rawdata = permute(rawdata,[3,2,1,4]); % Dimension convention of BART
k = permute(k,[2,1,3]); % Dimension convention of BART
[nFE,nSpokes,nCh] = size(rawdata);
imwidth = 300;

%% reconstruction parameters
maxitCG = 100;
alpha = 0.2;
tol = 1e-6;
display = 0;

%% simple density compensation for radial
k = k/(2*max(abs(k(:))));
w = ((abs(k))*2)+1e-3;

%% coil sensitivities
FT = NUFFT(k,w,1,0,[imwidth,imwidth], 2);
rawdatat = reshape(rawdata,[nFE*nSpokes,nCh]);
img_sens = zeros(imwidth,imwidth,nCh);
for ii=1:nCh
    img_sens(:,:,ii) = (FT'*(rawdatat(:,ii).*sqrt(w(:))));
end
img_sens_sos = sqrt(sum(abs(img_sens).^2,3));
mask = img_sens_sos > max(img_sens_sos(:))/5;
mask = imdilate(mask,strel('diamond',2));
[img,senseEst] = ...
            adapt_array_2d(img_sens(:,:,:));
senseEst = senseEst.*repmat(mask,[1,1,nCh]);

%% loop over subsampling

img = zeros([imwidth,imwidth,2,4]);
niter = zeros(4,1);
for R = [1,2,3,4] %[2,3,4]

    %% Subsampling
    kR = k(:,1:R:nSpokes);
    rawdataR = rawdata(:,1:R:nSpokes,:);
    wR = w(:,1:R:nSpokes)*R;
    [~,nSpokes,~]=size(rawdata);
    rawdataR = reshape(rawdataR,[nFE*floor(nSpokes/R),nCh]);
       
    FT = NUFFT(kR,wR,1,0,[imwidth,imwidth], 2);
    
    %% recon single coil
    img(:,:,1,R) = FT'*(rawdataR(:,1).*sqrt(wR(:)));
    
    %% CG recon, 1 iter
    img(:,:,2,R) = cg_sense_2d(rawdataR.*repmat(sqrt(wR(:)),[1,nCh]),...
        FT,senseEst,1,...
        alpha,tol,1,display);
    
    %% CG recon, 
    [img(:,:,3,R),delta] = cg_sense_2d(rawdataR.*repmat(sqrt(wR(:)),[1,nCh]),...
        FT,senseEst,1,...
        alpha,tol,maxitCG,display);
    niter(R) = length(delta);
end;

%% plot data
nR = size(img,4);
figure('Position',[500,500,500,640],'Color','w');
p = panel();
p.pack(nR,3);
p.de.margin = 1;
p.margin = [10,10,10,10];
headers = {'Single coil';'Initial';'Final'};
for i = 1:nR
    for j = 1:3
        p(i,j).select();
        imshow(fliplr(flipud(abs(img(:,:,j,i)))),[]);colormap bone;axis equal;
        if (j == 2)
            text(imwidth*0.8,imwidth*0.9,num2str(1),'Color','w','FontSize',14);
        end;
        if (j == 3)
            text(imwidth*0.8,imwidth*0.9,num2str(niter(i)),'Color','w','FontSize',14);
        end;
        if (j == 1)
            text(-imwidth*0.15,imwidth*0.5,num2str(i),'Color','k','FontSize',14);            
            if (i == R)
                text(0,imwidth*1.06,...
                    ['ISMRM 2019 Reproducibility challenge,' ...
                    'after Pruessmann, MRM 2001, fig 5'],'Color','k','FontSize',8);                
                text(0,imwidth*1.16,...
                    ['Steven Baete, NYU LMC CAI2R CBI'],'Color','k','FontSize',8);
            end;
        end;
        if (i == 1)
            text(imwidth*0.2,-imwidth*0.1,headers{j},'Color','k','FontSize',14);
        end;
    end;
end;
print('-dpng','braindata_fig5.png');
