%% ISMRM 2019 reproducible research study group
%  2019 reproduce a seminal paper initiative
% https://blog.ismrm.org/2019/04/02/ismrm-reproducible-research-study-group-2019-reproduce-a-seminal-paper-initiative/

% By: Steven Baete (steven.baete@nyulangone.org)
%     NYU LMC CBI/CAI2R 
%     April 2019

% Reproduce figure 6 of [1] with the cardiac data supplied. 
%
% [1] Pruessmann, K. P.; Weiger, M.; Boernert, P. and Boesiger, P.
% Advances in sensitivity encoding with arbitrary k-space trajectories.
% Magn Reson Med 46: 638-651 (2001)

clear all; clc; close all; 

restoredefaultpath
% Path to toolbox Jeff Fessler for NUFFT
path(path,genpath('irt/'));

% local scripts
path(path,genpath('gpuNUFFT-master/'));
path(path,'panel/');

%% Load data
rawdata_real    = h5read('rawdata_heart_radial_55proj_34ch.h5','/rawdata');
trajectory      = h5read('rawdata_heart_radial_55proj_34ch.h5','/trajectory');

rawdata = rawdata_real.r+1i*rawdata_real.i; clear rawdata_real;
k = trajectory(:,:,1) + 1j* trajectory(:,:,2);
rawdata = permute(rawdata,[3,2,1,4]); % Dimension convention of BART
k = permute(k,[2,1,3]); % Dimension convention of BART
[nFE,nSpokes,nCh] = size(rawdata);
imwidth = 300;

%% reconstruction parameters
maxitCG = 100;
alpha = 0.2;
tol = 1e-6;
display = 0;

%% simple density compensation for radial
k = k/(2*max(abs(k(:))));
w = ((abs(k))*2)+1e-3;

%% coil sensitivities
FT = NUFFT(k,w,1,0,[imwidth,imwidth], 2);
rawdatat = reshape(rawdata,[nFE*nSpokes,nCh]);
img_sens = zeros(imwidth,imwidth,nCh);
for ii=1:nCh
    img_sens(:,:,ii) = (FT'*(rawdatat(:,ii).*sqrt(w(:))));
end
img_sens_sos = sqrt(sum(abs(img_sens).^2,3));
mask = img_sens_sos > max(img_sens_sos(:))/50;
mask = imdilate(mask,strel('diamond',2));
[img,senseEst] = ...
            adapt_array_2d(img_sens(:,:,:));
senseEst = senseEst.*repmat(mask,[1,1,nCh]);

%% loop over subsampling
nproj = [55,33,22,11];
img = zeros([imwidth,imwidth,2,length(nproj)]);
for R = 1:length(nproj)

    %% Subsampling
    kR = k(:,1:nproj(R));
    rawdataR = rawdata(:,1:nproj(R),:);
    wR = w(:,1:nproj(R))*nproj(1)/nproj(R);
    [~,nSpokes,~]=size(rawdata);
    rawdataR = reshape(rawdataR,[nFE*floor(nproj(R)),nCh]);
       
    FT = NUFFT(kR,wR,1,0,[imwidth,imwidth], 2);
    
    %% conventional recon
    imgt = zeros(imwidth,imwidth,nCh);
    for j = 1:nCh
        imgt(:,:,j) = FT'*(rawdataR(:,j).*sqrt(wR(:)));
    end;
    img(:,:,1,R) = adapt_array_2d(imgt,eye(nCh),0);
    %figure;imagesc(abs(img(:,:,1,R)));colormap bone;
         
    %% CG recon, 
    img(:,:,2,R) = cg_sense_2d(rawdataR.*repmat(sqrt(wR(:)),[1,nCh]),...
        FT,senseEst,1,...
        alpha,tol,maxitCG,display);
end;

%% plot data
nR = size(img,4);
figure('Position',[500,500,1150,600],'Color','w');
p = panel();
p.pack(2,nR);
p.de.margin = 1;
p.margin = [10,10,10,10];
for i = 1:2
    for j = 1:nR
        p(i,j).select();
        if (~(i == 1 & (j == 1 | j == 3)))
            imgt = fliplr(flipud(abs(img(:,:,i,nR+1-j))));
            % crop for focus on the heart
            imgt = imgt(70:250,90:270);
            imw = size(imgt);
            imshow(imgt,[]);colormap bone;axis equal;
            text(imw(1)*0.55,imw(2)*0.93,[num2str(nproj(nR+1-j)) ' shots'],'Color','w','FontSize',20);            
        else
            axis off;
        end;
        if (i == 1 & j == 2)
            text(imw(1)*0.05,imw(2)*0.93,'a','Color','w','FontSize',26);   
        end;
        if (i == 2 & j == 1)
            text(imw(1)*0.05,imw(2)*0.93,'b','Color','w','FontSize',26);   
            text(0,imw(2)*1.04,...
                ['ISMRM 2019 Reproducibility challenge,' ...
                'after Pruessmann, MRM 2001, fig 6'],'Color','k','FontSize',14);                
            text(0,imw(2)*1.11,...
                ['Steven Baete, NYU LMC CAI2R CBI'],'Color','k','FontSize',14);
        end;
    end;
end;
print('-dpng','cardiacdata_fig6.png');
