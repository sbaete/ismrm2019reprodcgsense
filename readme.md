# ISMRM 2019 Reproducible research study group challenge: reproduce a seminal paper initiative

Steven Baete [steven . baete (at) nyulangone . org]

### Description
Submission to the [2019 seminal paper initiative](https://blog.ismrm.org/2019/04/02/ismrm-reproducible-research-study-group-2019-reproduce-a-seminal-paper-initiative/).

This submission leans heavily on the [NUFFT toolbox](http://web.eecs.umich.edu/~fessler/) by Jeff Fessler and the Matlab-portion of the [gpuNUFFT-package](https://github.com/andyschwarzl/gpuNUFFT) by Andreas Schwarzl and Florian Knoll.

### Contents
Matlab-scripts to generate figures 4,5 an 6 of [1] with the data provided for the challenge. The scripts were run on Matlab R2017a on a Linux Mint 18.2 operating system.

#### Figure 4: brain CG convergence
* braindata_fig4.m
* braindata_fig4.png
* Note: this figure does not come out quite as I want or expect it to be. I have spent some time, but cannot work out if I used the wrong image for the exact solution ('highly accurate approximation determined by extensive iteration' [1]), if the NUFFT I used is not an exact unitary operator, if there is a problem with the scaling of the dcf, or if there is an error in my algorithms. Edit after submission: I have since improved on this by calculating better b1-maps and optimizing the Tikhonov regularization parameter.

![Brain CG convergence](braindata_fig4.png)

#### Figure 5: brain images
* braindata_fig5.m
* braindata_fig5.png

![Brain images](braindata_fig5.png)

#### Figure 6: cardiac images
* cardiacdata_fig6.m
* cardiacdata_fig6.png

![Cardiac images](cardiacdata_fig6.png)

### Supporting packages
Only relevant portions of the toolboxes were added to this repository.

* [NUFFT toolbox](http://web.eecs.umich.edu/~fessler/) by Jeff Fessler in irt/ .
* [gpuNUFFT-package](https://github.com/andyschwarzl/gpuNUFFT) by Andreas Schwarzl and Florian Knoll in gpuNUFFT-master/ . This contains the NUFFT-operator by Ricardo Otazo. Both the NUFFT-operator and the cg_sense_2d.m script were modified.
* [Panel](https://www.mathworks.com/matlabcentral/fileexchange/20003-panel)-script by Ben Mitch from the Mathworks Matlab Central.

### References

[1] Pruessmann, K. P.; Weiger, M.; Boernert, P. and Boesiger, P. Advances in sensitivity encoding with arbitrary k-space trajectories. Magn Reson Med 46: 638-651 (2001)

Steven Baete [steven . baete (at) nyulangone . org]

Department of Radiology, Center for Biomedical Imaging,  
[Center for Advance Imaging Innovation and Research](cai2r.net),   
New York University School Of Medicine, New York, NY, USA.

PLEASE NOTE:

The software available on this page is provided free of charge and comes without any warranty. CAI²R and the NYU School of Medicine do not take any liability for problems or damage of any kind resulting from the use of the files provided. Operation of the software is solely at the user’s own risk. The software developments provided are not medical products and must not be used for making diagnostic decisions.

The software is provided for non-commercial, academic use only. Usage or distribution of the software for commercial purpose is prohibited. All rights belong to the author (Steven Baete) and the NYU School of Medicine. If you use the software for academic work, please give credit to the author in publications and cite the related publications.