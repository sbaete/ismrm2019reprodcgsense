%% ISMRM 2019 reproducible research study group
%  2019 reproduce a seminal paper initiative
% https://blog.ismrm.org/2019/04/02/ismrm-reproducible-research-study-group-2019-reproduce-a-seminal-paper-initiative/

% By: Steven Baete (steven.baete@nyulangone.org)
%     NYU LMC CBI/CAI2R 
%     April 2019

% Reproduce figure 4 of [1] with the brain data supplied. 
%
% [1] Pruessmann, K. P.; Weiger, M.; Boernert, P. and Boesiger, P.
% Advances in sensitivity encoding with arbitrary k-space trajectories.
% Magn Reson Med 46: 638-651 (2001)

clear all; clc; close all; 

restoredefaultpath
% Path to toolbox Jeff Fessler for NUFFT
path(path,genpath('irt/'));

% local scripts
path(path,genpath('gpuNUFFT-master/'));
path(path,'panel/');

%% Load data
rawdata_real    = h5read('rawdata_brain_radial_96proj_12ch.h5','/rawdata');
trajectory      = h5read('rawdata_brain_radial_96proj_12ch.h5','/trajectory');

rawdata = rawdata_real.r+1i*rawdata_real.i; clear rawdata_real;
k = trajectory(:,:,1) + 1j* trajectory(:,:,2);
rawdata = permute(rawdata,[3,2,1,4]); % Dimension convention of BART
k = permute(k,[2,1,3]); % Dimension convention of BART
[nFE,nSpokes,nCh] = size(rawdata);
imwidth = 300;

%% reconstruction parameters
maxitCG = 100;
alpha = 0.2;
tol = 1e-3;
display = 0;

%% simple density compensation for radial
k = k/(2*max(abs(k(:))));
w = ((abs(k))*2)+1e-3;

%% coil sensitivities
FT = NUFFT(k,w,1,0,[imwidth,imwidth], 2);
rawdatat = reshape(rawdata,[nFE*nSpokes,nCh]);
img_sens = zeros(imwidth,imwidth,nCh);
for ii=1:nCh
    img_sens(:,:,ii) = (FT'*(rawdatat(:,ii).*sqrt(w(:))));
end
img_sens_sos = sqrt(sum(abs(img_sens).^2,3));
mask = img_sens_sos > max(img_sens_sos(:))/5;
mask = imdilate(mask,strel('diamond',2));
[img,senseEst] = ...
            adapt_array_2d(img_sens(:,:,:));
      
%% calculate reference
[ref,a,b] = cg_sense_2d(reshape(rawdata,[nFE*nSpokes,nCh]).*repmat(sqrt(w(:)),[1,nCh]),...
    FT,senseEst,1,...
    alpha,tol,maxitCG*4,display);
        
%% loop over subsampling
R = [1,2,3,4];

for j = 1:length(R)
    
    %% Subsampling
    kR = k(:,1:R(j):nSpokes);
    rawdataR = rawdata(:,1:R(j):nSpokes,:);
    wR = w(:,1:R(j):nSpokes)*R(j);
    [~,nSpokes,~]=size(rawdata);
    rawdataR = reshape(rawdataR,[nFE*floor(nSpokes/R(j)),nCh]);
       
    FT = NUFFT(kR,wR,1,0,[imwidth,imwidth], 2);
       
    %% CG recon, 
    [~,deltaR,u_allR] = cg_sense_2d(rawdataR.*repmat(sqrt(wR(:)),[1,nCh]),...
        FT,senseEst,1,...
        alpha,tol,maxitCG,display);
    
    %% calculate image error
    ut = reshape(u_allR,[imwidth^2,maxitCG+1]);
    for i = 1:(maxitCG+1)
        tmp = ut(:,i) - ref(:);
        DeltaR(i) = (tmp'*tmp)/(ref(:)'*ref(:));
    end;

    %% save data
    delta(:,j) = deltaR;
    Delta(:,j) = DeltaR;
end;

%% plot data
figure('Position',[500,500,500,640],'Color','w');
p = panel();
p.pack(2,1);
p.de.margin = 8;
p.margin = [20,20,10,10];
linsty = {'-k';'--k';'-.k';':k'};

p(1,1).select();hold on;
for j = 1:length(R)
    plot(0:maxitCG,log10(Delta(:,j)),linsty{j});
end;
xlim([-1 maxitCG+1]);ylim([-7.0 0.2]);
box on;ylabel('log_{10}\Delta');
p(2,1).select();hold on;
for j = 1:length(R)
    plot(0:maxitCG,log10(delta(:,j)),linsty{j});
end;
box on;ylabel('log_{10}\delta');
legend('R = 1','R = 2','R = 3','R = 4','Location','NorthEast');legend boxoff;
xlim([-1 maxitCG+1]);ylim([-20.0 0.2]);
text(-2,-22.2,...
    ['ISMRM 2019 Reproducibility challenge,' ...
    'after Pruessmann, MRM 2001, fig 4'],'Color','k','FontSize',8);                
text(-2,-23.2,...
    ['Steven Baete, NYU LMC CAI2R CBI'],'Color','k','FontSize',8);

print('-dpng','braindata_fig4.png');
